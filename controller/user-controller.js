import db from '../src/db/db.js';
const user = db.userModal;

export const getUserById = async (req, res) => {
  try {
    const userDetail = await user.findById(req.params.id);
    if (userDetail !== null) {
      res.status(200).json({
        status: true,
        data: userDetail
      });
    } else {
      res.status(404).json({
        status: false,
        data: 'user not found'
      });
    }
  } catch (err) {
    res.status(400).json({
      status: false,
      message: err
    });
  }
};

export const updatePassword = async (req, res, next) => {
  try {
    const userDetail = await user.findById(req.params.id).select('+password');

    const validPassword = await userDetail.checkPassword(
      req.body.currentPassword,
      userDetail.password
    );

    if (!validPassword) {
      return next(
        res
          .status(400)
          .json({ status: false, message: 'current password is wrong' })
      );
    }

    userDetail.password = req.body.password;

    await userDetail.save();
    res.status(200).json({
      status: true,
      message: 'updated sucessfully'
    });
  } catch (err) {
    res.status(404).json({
      status: false,
      message: err
    });
  }
};

export const getAllUser = async (req, res) => {
  try {
    const userDetail = await user.find();
    res.status(200).json({
      status: true,
      data: userDetail
    });
  } catch (err) {
    res.status(404).json({
      status: false,
      error: err
    });
  }
};
