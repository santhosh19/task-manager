import db from '../src/db/db.js';
import jwt from 'jsonwebtoken';
import { sendEmail } from '../src/utils/sendMail.js';
import crypto from 'crypto';

const user = db.userModal;

export const signup = async (req, res) => {
  try {
    const userDetail = await user.create(req.body);
    createToken(userDetail, res);
  } catch (e) {
    res.status(201).json({
      status: false,
      message: e
    });
  }
};

const loginToken = (id) => {
  return jwt.sign({ id: id }, 'secret-key-for-jwt-token', {
    expiresIn: '1d'
  });
};

const createToken = (userDetails, res) => {
  const token = loginToken(userDetails._id);
  res.status(200).json({
    status: true,
    user: userDetails,
    authToken: token
  });
};

export const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    if (!email && !password) {
      return next();
    }

    const userDetails = await user.findOne({ email }).select('+password');

    const validPassword = await userDetails.checkPassword(
      password,
      userDetails.password
    );

    if (!validPassword) {
      return next(
        res.status(400).json({ status: false, message: 'password is wrong' })
      );
    }
    createToken(userDetails, res);
  } catch (e) {
    res.status(400).json({
      status: false,
      message: 'email or password is wrong'
    });
  }
};

export const forgotPassword = async (req, res, next) => {
  const userDetail = await user.findOne({ email: req.body.email });

  if (userDetail === null) {
    return next(
      res.status(404).json({ status: false, message: 'Email not exist' })
    );
  }
  // Generate random reset token
  const resetToken = userDetail.createPasswordResetToken();
  await userDetail.save({ validateBeforeSave: false });

  const resetURL = `${req.protocol}://${req.get(
    'host'
  )}/api/v1/auth/resetpassword/?token=${resetToken}`;

  const message = `click on below link to reset password: \n \n${resetURL}.\n`;
  try {
    await sendEmail({
      email: userDetail.email,
      subject: 'your password reset token (valid for 10min)',
      message
    });
    res.status(200).json({
      status: 'success',
      message: 'Reset link sent to email..!'
    });
  } catch (err) {
    userDetail.passwordResetToken = undefined;
    userDetail.passwordResetExpires = undefined;
    await userDetail.save({ validateBeforeSave: false });
    res.status(400).json({
      status: 'false',
      message: err
    });
  }
};

export const resetPassword = async (req, res, next) => {
  const resetToken = crypto
    .createHash('sha256')
    .update(req.body.token)
    .digest('hex');

  const userDetail = await user.findOne({
    passwordResetToken: resetToken,
    passwordResetExpires: { $gt: Date.now() }
  });

  if (userDetail === null) {
    return next(
      res.status(400).json({ status: false, message: 'Token is invalid' })
    );
  }

  userDetail.password = req.body.password;
  userDetail.passwordConfirm = req.body.passwordConfirm;
  userDetail.passwordResetToken = undefined;
  userDetail.passwordResetExpires = undefined;
  await userDetail.save();
  res.status(200).json({
    status: true,
    message: 'reset password successfully'
  });
};

export const protect = async (req, res, next) => {
  try {
    let token;
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer')
    ) {
      token = req.headers.authorization.split(' ')[1];
    }

    if (token === undefined) {
      return next(res.status(401));
    }

    const decoded = await jwt.verify(token, 'secret-key-for-jwt-token');
    const currentUser = await user.findOne({ _id: decoded.id });

    if (!currentUser) {
      return next();
    }

    // if (currentUser.changedPasswordAfter(decoded.iat)) {
    //   return next(
    //     res.sendStatus('User recently changed password..! please login again..!',
    //     401
    //   );
    // }
    // Grant access to protect user

    req.user = currentUser;
    next();
  } catch (err) {
    res.status(401).json({
      status: false,
      message: err
    });
  }
};

export const restrict = (...role) => {
  return (req, res, next) => {
    if (!role.includes(req.user.role)) {
      return next(
        res.status(400).json({
          status: false,
          message: 'User does not have permission'
        })
      );
    }
    return next();
  };
};
