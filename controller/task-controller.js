import db from '../src/db/db.js';
import {
  getTotalTask,
  getPerformanceTask
} from '../src/repositary/taskRepo.js';

const task = db.taskModal;

export const createTask = async (req, res) => {
  try {
    const newTask = await task.create(req.body);
    res.status(201).send({
      status: true,
      message: 'task created successfully',
      data: newTask
    });
  } catch (e) {
    res.status(404).send({
      status: false,
      message: e
    });
  }
};

export const updateTask = async (req, res) => {
  try {
    const taskUpdate = await task.findByIdAndUpdate(req.params.id, req.body);
    res.status(201).send({
      status: true,
      message: 'task updated successfully',
      data: taskUpdate
    });
  } catch (e) {
    res.status(404).send({
      status: false,
      message: e
    });
  }
};

export const deleteTask = async (req, res) => {
  try {
    const taskDelete = await task.findByIdAndDelete(req.params.id);
    if (taskDelete != null) {
      res.status(200).send({
        status: true,
        message: 'task deleted successfully',
        data: taskDelete
      });
    } else {
      res.status(404).send({
        status: true,
        message: 'data not found'
      });
    }
  } catch (e) {
    res.status(404).send({
      status: false,
      message: e
    });
  }
};

export const getAllTask = async (req, res) => {
  try {
    const getTask = await task.find();
    res.status(200).send({
      status: true,
      data: getTask,
      total: await getTotalTask()
    });
  } catch (e) {
    res.status(404).send({
      status: false,
      message: e
    });
  }
};

export const getPerformance = async (req, res) => {
  try {
    const performance = await getPerformanceTask(req.body);
    const total = await getTotalTask();
    res.status(200).send({
      status: true,
      totalTask: total,
      data: performance
    });
  } catch (err) {
    res.status(404).json({
      status: false,
      error: err
    });
  }
};
