import db from '../src/db/db.js';
const project = db.projectModal;
import { getTotalProject } from '../src/repositary/projectRepo.js';

export const getAllProject = async (req, res) => {
  try {
    const searchKey = req.query.search;
    const pagination = {
      pageNumber: req.query.pageNumber,
      pageSize: req.query.pageSize
    };
    var page = pagination.pageNumber > 0 ? pagination.pageNumber - 1 : 0;

    var total = await getTotalProject();

    const projects = await project
      .find()
      .skip(parseInt(page * pagination.pageSize))
      .limit(parseInt(pagination.pageSize));

    res.status(200).json({
      status: true,
      totalProject: total,
      data: projects,
      pageNumber: parseInt(pagination.pageNumber),
      pageSize: parseInt(pagination.pageSize),
      totalCount: total
    });
  } catch (err) {
    res.status(404).json({
      status: false,
      message: err
    });
  }
};

export const createProject = async (req, res) => {
  try {
    await project.create(req.body);
    res.status(201).json({
      status: true,
      message: 'data created successfully'
    });
  } catch (err) {
    res.status(404).json({
      status: false,
      message: err
    });
  }
};
