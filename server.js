import app from './app.js';
import db from './src/db/db.js';
import { port } from './config/index.js';

const user = db.userModal;

const adminData = {
  name: 'admin',
  email: 'admin123@gmail.com',
  password: 'Admin@123',
  confirmPassword: 'Admin@123',
  role: 'admin'
};

const importData = async () => {
  try {
    await user.create(adminData);
    //process.exit();
  } catch (err) {
    console.info(err);
  }
};

const deleteData = async () => {
  try {
    await user.deleteMany();
    //process.exit();
  } catch (err) {
    console.info(err);
  }
};

if (process.argv[2] === '-import') {
  importData();
} else if (process.argv[2] === '-delete') {
  deleteData();
}

app.listen(port, (req, res) => {
  console.info(`Server is running in this ${port}`);
});
