import mongoose from 'mongoose';
import validator from 'validator';
import bcrypt from 'bcrypt';
import crypto from 'crypto';
const userSchema = mongoose.Schema(
  {
    name: {
      type: String
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      validate: [validator.isEmail, 'email is not valid']
    },
    password: {
      type: String
    },
    role: {
      type: String,
      enum: ['user', 'admin'],
      default: 'user'
    },
    confirmPassword: {
      type: String,
      validate: function (el) {
        return el === this.password;
      }
    },
    passwordChangedAt: Date,
    passwordResetToken: String,
    passwordResetExpires: Date
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

userSchema.set('toJSON', {
  transform: (doc, ret, opt) => {
    if (!!ret.password) delete ret.password;
    if (ret.__v != undefined) delete ret.__v;
    return ret;
  },
  getters: true,
  virtuals: true
});

userSchema.pre('save', async function (next) {
  const salt = bcrypt.genSaltSync();
  this.password = await bcrypt.hash(this.password, salt);
  this.confirmPassword = undefined;
  next();
});

userSchema.methods.checkPassword = async function (
  candidatePassword,
  userPassword
) {
  return await bcrypt.compare(candidatePassword, userPassword);
};

userSchema.methods.createPasswordResetToken = function () {
  const resetToken = crypto.randomBytes(32).toString('hex');

  this.passwordResetToken = crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');

  this.passwordResetExpires = Date.now() + 10 * 60 * 1000;

  return resetToken;
};

userSchema.set('toJSON', {
  transform: (doc, ret, opt) => {
    if (ret != undefined) {
      delete ret.password;
      delete ret.__v;
    }
    return ret;
  },
  getters: true,
  virtuals: true
});

const userModel = mongoose.model('user', userSchema);

export default userModel;
