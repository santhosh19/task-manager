import mongoose from 'mongoose';

const taskSchema = mongoose.Schema(
  {
    taskName: {
      type: String
    },
    projectName: {
      type: String
    },
    startDate: {
      type: Date,
      unique: true
    },
    endDate: {
      type: Date,
      unique: true
    },
    status: {
      type: String,
      lowercase: true
    }
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

const taskModel = mongoose.model('task', taskSchema);

export default taskModel;
