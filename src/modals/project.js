import mongoose from 'mongoose';

const projectSchema = mongoose.Schema(
  {
    projectName: {
      type: String
    },
    projectDescription: {
      type: String
    },
    startDate: {
      type: Date
    },
    endDate: {
      type: Date
    },
    developer: {
      type: String
    },
    status: {
      type: String,
      lowercase: true
    }
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

const projectModal = mongoose.model('project', projectSchema);

export default projectModal;
