import nodemailer from 'nodemailer';

export const sendEmail = async (options) => {
  //1) create a transporter

  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: '', //Add email here
      pass: '' //add password here
    }
  });

  const mailOption = {
    from: 'santhoshmanian1997@gmail.com',
    to: options.email,
    subject: options.subject,
    text: options.message
  };

  await transporter.sendMail(mailOption);
};
