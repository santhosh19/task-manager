import db from '../db/db.js';

const projects = db.projectModal;

export const getTotalProject = async () => {
  const totalProject = await projects.countDocuments();
  return totalProject;
};
