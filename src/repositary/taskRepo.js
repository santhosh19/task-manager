import db from '../db/db.js';

const tasks = db.taskModal;

export const getPerformanceTask = async (status) => {
  const totalTask = await tasks.aggregate([
    {
      $match: status
    },
    { $unset: ['_id', '__v', 'endDate', 'created_at', 'updated_at'] }
  ]);
  return totalTask;
};

export const getTotalTask = async () => {
  const total = await tasks.countDocuments();
  return total;
};
