import express from 'express';
import {
  getAllTask,
  createTask,
  updateTask,
  deleteTask,
  getPerformance
} from '../../controller/task-controller.js';
import { protect, restrict } from '../../controller/auth-controller.js';

const taskRoute = express.Router();

taskRoute
  .get('/taskDetails', protect, getAllTask)
  .post('/performance', protect, getPerformance)
  .post('/createTask', protect, createTask);

taskRoute
  .route('/:id')
  .patch(protect, updateTask)
  .delete(protect, restrict('admin'), deleteTask);

export default taskRoute;
