import express from 'express';
import {
  signup,
  login,
  forgotPassword,
  resetPassword
} from '../../controller/auth-controller.js';

const authRoute = express.Router();

authRoute.post('/signup', signup);
authRoute.post('/login', login);
authRoute.post('/forgotpassword', forgotPassword);
authRoute.post('/resetPassword', resetPassword);

export default authRoute;
