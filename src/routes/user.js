import express from 'express';

import {
  getAllUser,
  getUserById,
  updatePassword
} from '../../controller/user-controller.js';
import { protect } from '../../controller/auth-controller.js';

const userRoute = express.Router();

userRoute.get('/', protect, getAllUser);
userRoute.route('/:id').get(protect, getUserById).post(protect, updatePassword);

export default userRoute;
