import express from 'express';

import {
  getAllProject,
  createProject
} from '../../controller/project-controller.js';

import { protect } from '../../controller/auth-controller.js';

const projectRoute = express.Router();

projectRoute.get('/project', protect, getAllProject);

projectRoute.post('/createProject', protect, createProject);

export default projectRoute;
