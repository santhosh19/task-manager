import taskModal from '../modals/task.js';
import userModal from '../modals/user.js';
import projectModal from '../modals/project.js';

import mongoose from 'mongoose';

mongoose
  .connect('mongodb://localhost:27017/task-manager', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true
  })
  .then(() => {
    console.info('DB connection successfully');
  });

export default { taskModal, userModal, projectModal };
