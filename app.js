import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import taskRoute from './src/routes/task.js';
import authRoute from './src/routes/auth.js';
import projectRoute from './src/routes/project.js';
import userRoute from './src/routes/user.js';
import bodyParser from 'body-parser';
const app = express();

app.use(cors());
app.use(cookieParser());

app.use(bodyParser.json());

app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

app.use(function (req, res, next) {
  res.setHeader(
    'Access-Control-Allow-Methods',
    'POST, PUT, OPTIONS, DELETE, GET'
  );
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.header('Access-Control-Allow-Credentials', true);
  next();
});

app.use('/api/v1/task/', taskRoute);
app.use('/api/v1/auth/', authRoute);
app.use('/api/v1/dashboard/', projectRoute);
app.use('/api/v1/user/', userRoute);

export default app;
